import time
import sys
import utils.main as main
import utils.mailer as mailer

# runs forver
while(1>0):
    
    response = main.run()

    # this send email if bot bot on Raspberry Pi Crashes...?
    print ('Send error email?: '+str(not response))
    
    #if the process came back with an error... send email
    if response==False:
        mailer.sendEmail()
    print ('Restarting Loop in 60s')    
    print ('')
    time.sleep(60)