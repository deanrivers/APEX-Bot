


Random update
# APEX-Bot

This bot runs using Python. 

The main file to look at is process.py. 

That is where everything happens. This bot runs in an infinite loop that can be found in ApexStatsBot.py.

## Run Locally
1. `pip install -r requirements.txt`
2. `python3 ApexStatsBot.py`

You should now see your program running indefinitely.

## Tests

This app has unit tests!

`python -m unittest -v utils/unit_tests.py`
