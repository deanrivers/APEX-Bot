import json
import oauth2 as oauth
import tweepy
import utils.cfg as cfg
import threading
import time
import requests
import urllib3
from requests.auth import HTTPBasicAuth
import datetime
import sys
import utils.createPlot as cp
import os

# setup
# twitter validation
auth = tweepy.OAuthHandler(cfg.consumer_key, cfg.consumer_secret)
auth.set_access_token(cfg.access_token, cfg.access_token_secret)
api = tweepy.API(auth)

# APEX API key
headers = cfg.headers

# setup credentials to access Twitter mentions via API
consumer = oauth.Consumer(key=cfg.consumer_key, secret=cfg.consumer_secret)
access_token = oauth.Token(key=cfg.access_token, secret=cfg.access_token_secret)
client = oauth.Client(consumer, access_token)

# write last since_id to since_ids.txt
# this ID is used to deteremine if there are any new tweets that need to be processed
def updateSinceIDs(tweetID):
    f = open("since_ids.txt", "a")
    f.write(str(tweetID) + "\n") 
    f.close()
    print('"since_ids.txt" Update triggered for: ' + str(tweetID))
    return True

# retrieve last since_id
# this will get the last ID of the tweets already processed
def retrieveLastID():
    # update since_id to the last line on initial call
    fileHandle = open ( 'since_ids.txt',"r" )
    lineList = fileHandle.readlines()
    fileHandle.close()
    print ("The last line value in the since_ids.txt is: ")

    # or simply
    lastID = lineList[-1]
    print(lastID)
    return lastID

# this is the main process
def run():
    # reference the last id in since_ids.txt
    last_id = retrieveLastID() 

    # twitter mentions API endpoint
    twitter_endpoint = 'https://api.twitter.com/1.1/statuses/mentions_timeline.json?screen_name=ApexLegendsBot&since_id='+last_id
    twitter_response, twitter_data = client.request(twitter_endpoint)

    # try to access API to get mentions
    try: 
        tweets = json.loads(twitter_data)    
        print(tweets)

    except Exception as e: 
        print('!!!!!!!!!!')
        print('Something went wrong when hitting the Twitter API')
        print(e)
        print('!!!!!!!!!!')
        return False

    print('Looking at tweets...')
    for ID in tweets:
        screen_name = ID['user']['screen_name']
        str_ID = ID['id']
        text = ID['text']
        print('Username: ' + screen_name)
        print(' ')
        print('Tweet: ' + text)
        print('string ID: ' + str(str_ID))

    for idx, tweet in enumerate(tweets):
        time.sleep(1)

        #update batchLastId and append to text file for every tweet    
        since_id = str(tweets[0]['id_str'])       

        text = tweet['text']
        screen_name = tweet['user']['screen_name']
        tweet_ID = tweet['id']

        print('')
        print('-----------Getting APEX Stats-----------')

        # parse tweet
        parsed_tweet = parse_tweet(text)

        # parsed_tweet = text.split('@ApexLegendsBot')[1].strip()
        print(parsed_tweet)

        # assign platform and username
        platform = parsed_tweet['platform']
        apex_username = parsed_tweet['username']

        print("Determined platform and username")
        print(platform)
        print(apex_username)

        apex_stats = fetch_apex_stats(platform,apex_username,screen_name,tweet_ID)

    if len(tweets) == 0:
        print ('Nothing new on initial start')
        since_id = last_id
    else:
        print ('Appending tweet id to since_ids.txt ' + last_id)
        updateSinceIDs(str(tweets[0]['id_str']))

    return True

# functions
def parse_tweet(text):
    text = text.split('@ApexLegendsBot')[1]
    platforms = ['xbl','psn','origin']
    tweet = dict()
    
    platform = text.split(':')[0].strip() if text.find(':') != -1 and text.split(':')[0] != '' and text.split(':')[0] != ' ' else 'invalid'
    username = text.split(':')[1].strip() if text.find(':') != -1 and text.split(':')[1] != '' and text.split(':')[1] != ' ' else 'invalid'
    print('')
    print("P:",platform, username)
    tweet['platform'] = platform.lower() if platform.lower() in platforms else 'invalid'
    tweet['username'] = username.lower()
    print('')
    print(tweet)
    return tweet

def post_tweet(string,tweet_ID):
        print ('')
        print ('-----------Posting Tweet-----------')
        if os.path.exists("apex_plot.png"):
            media = api.media_upload("apex_plot.png")
            api.update_status(string,in_reply_to_status_id=tweet_ID,media_ids=[media.media_id])
        else:
            api.update_status(string,in_reply_to_status_id=tweet_ID)
        print ('-----------Tweet Posted-----------')
        print('')

        if os.path.exists("apex_plot.png"):
            os.remove("apex_plot.png")
        else:
            print("The file does not exist")
        
def fetch_apex_stats(platform,username,screen_name,tweet_ID):
    # initialize tweet info
    print("tweet id")
    print(tweet_ID)
    tweet = ""
    x = datetime.datetime.now()
    dateDay = x.strftime("%x")
    dateTime = x.strftime("%X")

    if platform=="invalid" or username=="invalid":
        tweet = "Hey @"+screen_name +",\n\n"+"I do not understand your query...\n\n"+"Platform: "+platform+"\n"+"Username: "+username+"\n\n"+dateDay+" "+dateTime
        post_tweet(tweet,tweet_ID)
        return None

    print("----------Connecting to APEX----------")
    print("Platform:")
    print(platform)
    print("Username:")
    print(username)
    print("----------")
    print("")

    
    try:
        r = requests.get('https://public-api.tracker.gg/v2/apex/standard/profile/' + platform + '/' + username,headers=headers)
        print(r)
        stats = r.json()
        stats_array = {}
        statusCode = r.status_code 
        # print("stats from endpoint =>",stats) 

    except Exception as e:
        print('!!!!!!!!!!')
        print ('Something went wrong when hitting the Apex API.')
        print('!!!!!!!!!!')
        print ("Error =>",e)
        tweet = 'Hey @'+ screen_name +","+'\n'+"I tried to find '"+username+"' in my '"+platform+"' database but it was not there... \n\nplease make sure your query is correct. \n\nHave a nice day!"+'\n\n'+dateDay+' '+dateTime
        post_tweet(tweet,tweet_ID)
        return False
    # check status code
    print ('Status code: '+str(statusCode))
    print ('')


    if statusCode == 200:
        # create plot
        try:
            cp.plot(stats)
            time.sleep(3)
        except Exception as e:
            print('!!!!!!!!!!')
            print ('Something went wrong with creating the matplot.',e)
            print('!!!!!!!!!!')

        # populate stats dicitionary
        for info in stats['data']['segments'][0]['stats']:
            index = list(stats['data']['segments'][0]['stats']).index(info)

            # create key value pairs for the info we want to tweet.
            key = str(info).upper()
            value = stats['data']['segments'][0]['stats'][info]['displayValue']

            # append key value pair to dictionary.
            stats_array.update({key:value})
            
        # This string will be tweeted out and will hold the Apex info.
        string = ''
        print ('Stats Info Array:')
        print(stats_array)

        # get top 6 stats items for the tweet
        for count, (key, value) in enumerate(stats_array.items(), 0):
            string += key+'-----'+value+'\n'
            if count > 6:
                break

        # post tweet     
        tweet = 'Hey @'+ screen_name +", here are the top stats for '"+username + "':"   +'\n\n'+'Active Legend: '+str(stats['data']['metadata']['activeLegendName'])+'\n'+string+'\n'+dateDay+' '+dateTime
        post_tweet(tweet,tweet_ID)    

    elif statusCode == 400:
        print('Error 400: Bad information was provided')
    elif statusCode == 404:
        print ('Error 404: Nothing found...Tweet sent out.')
        
    elif statusCode == 500:
        print ('Error 500: Something is wrong on the APIs end')
    else: 
        print ('Error Unknown: The APEX API could not be accessed for some reason.')
    return None