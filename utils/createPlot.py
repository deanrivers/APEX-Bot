import matplotlib.pyplot as plt

def plot(stats):
    print('----------Creating Plot----------')
    overview_segment = stats['data']['segments'][0]["stats"]
    data = {}

    # populate data dicitionary
    for stat in stats['data']['segments'][0]['stats']:
        # create key value pairs for the info we want to tweet.
        key = stat
        value = stats['data']['segments'][0]['stats'][stat]['percentile'] if stats['data']['segments'][0]['stats'][stat]['percentile'] != None else 0.0
        # append key value pair to dictionary. limit to 5.
        if len(data)>6:
            break
        else:
            data.update({key:value})
        
    names = list(data.keys())
    values = list(data.values())
    apex_username = stats['data']['platformInfo']['platformUserId']

    # create plot
    plt.bar(names,values,color="#ff9469")
    plt.ylabel("Percentile")
    plt.yticks(fontsize=8)
    plt.xticks(fontsize=8, rotation=45)
    plt.ylim(0, 100)
    plt.title("Percentiles for: "+"'"+apex_username+"'")
    plt.tight_layout()

    # save image
    plt.savefig('apex_plot.png',bbox_inches='tight')
    print('----------Plot Image Saved----------')
