import unittest
from utils.main import parse_tweet
from utils.main import fetch_apex_stats
from utils.main import post_tweet

class TestParseTweet(unittest.TestCase):
    def test_parser(self):
        platforms = ['xbl','psn','origin']

        text_str_1 = '@ApexLegendsBot xbll:uhdidas'
        res_1 = parse_tweet(text_str_1)
        self.assertEqual(res_1['platform'],'invalid')

        text_str_2 = '@ApexLegendsBot xbl:'
        res_2 = parse_tweet(text_str_2)
        self.assertEqual(res_2['username'],'invalid')

        text_str_3 = '@ApexLegendsBot random xbl: of stuff'
        res_3 = parse_tweet(text_str_3)
        self.assertEqual(res_3['platform'],'invalid')

        text_str_4 = '@ApexLegendsBot random xbl::test'
        res_4 = parse_tweet(text_str_4)
        self.assertEqual(res_4['platform'],'invalid')

        text_str_5 = '@ApexLegendsBot origin:uhdidas'
        res_5 = parse_tweet(text_str_5)
        self.assertEqual(res_5['username'],'uhdidas')

        text_str_6 = '@ApexLegendsBot origin:nyke espy'
        res_6 = parse_tweet(text_str_6)
        self.assertEqual(res_6['platform'],'origin')

        text_str_7 = '@ApexLegendsBot origin:NyKe EsPy'
        res_7 = parse_tweet(text_str_7)
        self.assertEqual(res_7['username'],'nyke espy')

        text_str_8 = '@ApexLegendsBot OriGin:NyKe EsPy'
        res_8 = parse_tweet(text_str_8)
        self.assertEqual(res_8['platform'],'origin')

        text_str_9 = '@ApexLegendsBot OriGin:NyKe->EsPy'
        res_9 = parse_tweet(text_str_9)
        self.assertEqual(res_9['username'],'nyke->espy')

        text_str_9 = '@ApexLegendsBot OriGin:NyKe::EsPy'
        res_9 = parse_tweet(text_str_9)
        self.assertEqual(res_9['username'],'nyke')

if __name__ == '__main__':
    unittest.main()